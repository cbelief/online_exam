import Vue from "vue"
import Router from 'vue-router'
Vue.use(Router)


export default new Router({
    routes: [
        {
            path: '/',
            name: 'login', //登录界面
            component: () => import('@/components/common/Login')
        },
        {
            path: '/student',
            component: () => import('@/components/student/Index'),
            redirect: '/student/exams',
            children: [
                {path:"/student/exams",component: () => import('@/components/student/MyExams')},
                {path: '/student/examMsg', component: () => import('@/components/student/ExamMsg')},
                {path: '/student/practices',component: () => import('@/components/student/MyPractices')},
                {path: '/student/examLog',component: () => import('@/components/student/ExamLog')},
                {path: '/student/examLogDetail',component: () => import('@/components/student/ExamLogDetail')},
                {path: '/student/practiceLog',component: () => import('@/components/student/PracticeLog'),},
                {path: '/student/PracticeLogDetail',component: () => import('@/components/student/PracticeLogDetail')}
            ]
          },
        {
            path: '/answer',
            component: () => import('@/components/student/ExamAnswer')
        },
        {
            path: '/practice',
            component:() => import('@/components/student/PracticeAnswer')
        }
    ]
})