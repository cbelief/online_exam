import {
  reqStudentLogin,
} from "@/api";
import { getToken,removeToken} from "@/utils/token";

const state = {
  token: getToken(),
  userInfo: {},
};
const mutations = {
  USERLOGIN(state, userInfo) {
    state.userInfo = userInfo;
  },
  GETUSERINFO(state, userInfo) {
    state.userInfo = userInfo;
  },
  //清除本地数据
  CLEAR(state){

    state.token = '';
    state.userInfo={};
    //本地存储数据清空
    removeToken();
  }
};
const actions = {
  async studentLogin({ commit }, loginInfo) {
    let userName = loginInfo.userName;
    let passWord = loginInfo.passWord;
    let result = await reqStudentLogin(userName,passWord);
    if (result.code == 200) {
      commit("USERLOGIN", result.data);
      return result.data;
    } else {
      // return Promise.reject(result.message);
      return new Promise((resolve,reject)=>{
          reject(result.msg)
      })
    }
  },
};
const getters = {};
export default {
  state,
  mutations,
  actions,
  getters,
};
