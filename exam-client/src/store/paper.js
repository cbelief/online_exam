import {reqGetPaper} from "@/api";
  
  const state = {
    count: [],
    paper: {},
  };
  const mutations = {
    GETPAPER(state, paper){
      state.paper = paper;
      let keys = ['multi', 'fill', 'judge'];
      let count = [];
      keys.forEach(e => {
        let data  = paper[e];
        count.push(data.length);
      })
      state.count = count;
    }
  };
  const actions = {
    async getPaper({commit}, paperId) {
      let result = await reqGetPaper(paperId);
      if (result.code == 200) {
        commit("GETPAPER", result.data);
      }
    },
  };
  const getters = {};

  export default {
    state,
    mutations,
    actions,
    getters,
  };
  