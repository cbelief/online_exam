import axios from "axios";

let requests = axios.create({
  //基础路径
  baseURL: "http://127.0.0.1/api",
  //请求不能超过5S
  timeout: 5000,
});

//请求拦截器
requests.interceptors.request.use((config) => {
  //config:配置对象
  // if(store.state.detail.uuid_token){
  //   //请求头添加一个字段(userTempId)
  //   config.headers.userTempId = store.state.detail.uuid_token;
  // }
  // //需要携带token带给服务器
  // if(store.state.user.token){
  //   config.headers.token = store.state.user.token;
  // }
  return config;
});

//响应拦截器
requests.interceptors.response.use(
  (res) => {
    return res.data;
  },
  // eslint-disable-next-line no-unused-vars
  (err) => {
    alert("服务器响应数据失败");
  }
);
export default requests;
