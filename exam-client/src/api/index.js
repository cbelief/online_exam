import requests from "./ajax";

export const reqStudentLogin = (userName, passWord) => requests({ url: `/login?studentId=${userName}&passWord=${passWord}`, method: 'get' });

export const reqGetExamInfo = (search) => requests({url: '/exams', method: 'post', data: search})

export const reqGetExamByExamCode = (examCode) => requests({url: `/exam/${examCode}`, method: 'get'})

export const reqGetPaper = (paperId) => requests({url: `/paper/${paperId}`, method: 'get'})

export const reqCommitPaper = (commitPaper) => requests({url: '/paper/commit', method: 'post', data: commitPaper})

export const reqGetCourseInfo = (search) => requests({url: '/courses', method: 'post', data: search})

export const reqGetPractice = (courseName) => requests({url: `/practice?courseName=${courseName}`, method: 'get'})

export const reqCommitPractice = (commitPractice) => requests({url: '/practice/commit', method: 'post', data: commitPractice}) 

export const reqGetExamLogs = (search, studentId) => requests({url: `/exam/getLogs?studentId=${studentId}`, method: 'post', data: search})

export const reqGetExamLogDetail = (examRecordId) => requests({url: `/exam/getLogDetail?examRecordId=${examRecordId}`, method: 'get'})

export const reqGetPracticeLogs = (search, studentId) => requests({url: `/practice/getLogs?studentId=${studentId}`, method: 'post', data: search})

export const reqGetPracticeLogDetail = (practiceRecordId) => requests({url: `/practice/getLogDetail?practiceRecordId=${practiceRecordId}`})