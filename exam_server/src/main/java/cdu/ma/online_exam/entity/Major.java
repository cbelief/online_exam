package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class Major {
    private int majorId;
    private String majorName;
}
