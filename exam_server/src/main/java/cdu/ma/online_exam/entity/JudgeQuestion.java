package cdu.ma.online_exam.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=true)
@Data
public class JudgeQuestion extends Question{

    private String rightAnswer;

    private String studentAnswer;

}