package cdu.ma.online_exam.entity;

import java.util.Objects;

public class AnswerLog {
    private int questionId;
    private int questionType;
    private String studentAnswer;
    private String rightAnswer;

    public AnswerLog() {
    }

    public AnswerLog(int questionId, int questionType, String studentAnswer, String rightAnswer) {
        this.questionId = questionId;
        this.questionType = questionType;
        this.studentAnswer = studentAnswer;
        this.rightAnswer = rightAnswer;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnswerLog)) return false;
        AnswerLog answerLog = (AnswerLog) o;
        return questionId == answerLog.questionId &&
                questionType == answerLog.questionType &&
                Objects.equals(studentAnswer, answerLog.studentAnswer) &&
                Objects.equals(rightAnswer, answerLog.rightAnswer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId, questionType, studentAnswer, rightAnswer);
    }
}
