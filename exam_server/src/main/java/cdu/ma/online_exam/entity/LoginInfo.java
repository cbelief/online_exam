package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class LoginInfo {
    private String userName;
    private String passWord;
}
