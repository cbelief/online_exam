package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class MultiQuestion extends Question{

    private String optionA;

    private String optionB;

    private String optionC;

    private String optionD;

    private String rightAnswer;

    private String studentAnswer;

}