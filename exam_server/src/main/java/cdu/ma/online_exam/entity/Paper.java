package cdu.ma.online_exam.entity;

import lombok.Data;

import java.util.List;

@Data
public class Paper {
    private int paperId;
    private List<MultiQuestion> multi;
    private List<FillQuestion> fill;
    private List<JudgeQuestion> judge;
}
