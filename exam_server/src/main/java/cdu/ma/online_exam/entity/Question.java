package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class Question {
    private Integer questionId;

    private String course;

    private String question;

    private Integer score;

    private String section;

    private String analysis;

}
