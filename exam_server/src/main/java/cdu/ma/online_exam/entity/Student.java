package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class Student {
    private int studentId;
    private String studentName;
    private String gender;
    private String tel;
    private String email;
    private Major major;
    private Clazz clazz;
}
