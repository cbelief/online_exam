package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class FillQuestion extends Question{

    private String rightAnswer;

    private String studentAnswer;

}
