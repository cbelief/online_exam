package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class Course {
    private int courseId;
    private String courseName;
    private String description;
}
