package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class Clazz {
    private int clazzId;
    private int clazzName;
}
