package cdu.ma.online_exam.entity;

import lombok.Data;

@Data
public class Exam {
    private Integer examCode;

    private String description;

    private String course;

    private Integer paperId;

    private String examDate;

    private Integer totalTime;

    private String grade;

    private String term;

    private String major;

    private String institute;

    private Integer totalScore;

    private String type;

    private String tips;
}