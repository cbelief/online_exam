package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.FillQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface FillQuestionMapper {
    @Select("select f.question_id,f.`course`," +
            "f.question,f.score,f.section " +
            "from t_paper_question p join t_fill_question f " +
            "on p.paper_id = #{paperId} and p.question_type = 2 and p.question_id = f.question_id")
    List<FillQuestion> selectFillQuestionsByPaperID(int paperId);

    @Select("select f.question_id,f.`course`,f.question," +
            "f.right_answer,f.analysis,f.score,f.section " +
            "from t_paper_question p join t_fill_question f " +
            "on p.paper_id = #{paperId} and p.question_type = 2 and p.question_id = f.question_id")
    List<FillQuestion> selectFillQuestionsWithRightAnswer(int paperId);

    @Select("select * from t_fill_question where course = #{courseName}")
    List<FillQuestion> selectAllFillQuestionsByCourseName(String courseName);
}
