package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.MultiQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MultiQuestionMapper {
    @Select("select m.question_id,m.`course`,m.question,m.optionA,m.optionB," +
            "m.optionC,m.optionD,m.score,m.section " +
            "from t_paper_question p join t_multi_question m " +
            "on p.paper_id = #{paperId} and p.question_type = 1 and p.question_id = m.question_id")
    List<MultiQuestion> selectMultiQuestionsByPaperID(int paperId);

    @Select("select m.question_id,m.`course`,m.question,m.optionA,m.optionB,m.optionC,m.optionD," +
            "m.right_answer,m.analysis,m.score,m.section " +
            "from t_paper_question p join t_multi_question m " +
            "on p.paper_id = #{paperId} and p.question_type = 1 and p.question_id = m.question_id")
    List<MultiQuestion> selectMultiQuestionsWithRightAnswer(int paperId);

    @Select("select * from t_multi_question where course = #{courseName}")
    List<MultiQuestion> selectAllMultiQuestionsByCourseName(String courseName);
}
