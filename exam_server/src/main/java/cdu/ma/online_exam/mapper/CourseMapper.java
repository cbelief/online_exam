package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CourseMapper {
    @Select("<script>" +
                "select * from t_course" +
                "<where>" +
                "<if test=\"keyWord !=null and keyWord != ''\">" +
                    "course_name like '%${keyWord}%'" +
                "</if>" +
                "</where>" +
            "</script>")
    List<Course> SelectCourses(String keyWord);
}
