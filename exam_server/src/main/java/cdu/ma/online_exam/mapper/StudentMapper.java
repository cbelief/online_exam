package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.Student;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface StudentMapper {
    @Results(id = "studentMap", value = {
            @Result(id = true, property = "studentId", column = "student_id"),
            @Result(property = "studentName", column = "student_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "tel", column = "tel"),
            @Result(property = "email", column = "email"),
            @Result(property = "major", column = "major_id", one = @One(select = "cdu.ma.online_exam.mapper.MajorMapper.selectMajorById")),
            @Result(property = "clazz", column = "clazz_id", one = @One(select = "cdu.ma.online_exam.mapper.ClazzMapper.selectClazzById"))
    })
    @Select("select * from t_student where student_id = #{studentId} and pass_word = #{passWord}")
    Student selectStudentByIdAndPwd(@Param("studentId") int studentId, @Param("passWord") String passWord);
}
