package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.JudgeQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface JudgeQuestionMapper {
    @Select("select j.question_id,j.`course`," +
            "j.question,j.score,j.section " +
            "from t_paper_question p join t_judge_question j " +
            "on p.paper_id = #{paperId} and p.question_type = 3 and p.question_id = j.question_id")
    List<JudgeQuestion> selectJudgeQuestionsByPaperID(int paperId);

    @Select("select j.question_id,j.`course`,j.question," +
            "j.right_answer,j.analysis,j.score,j.section " +
            "from t_paper_question p join t_judge_question j " +
            "on p.paper_id = #{paperId} and p.question_type = 3 and p.question_id = j.question_id")
    List<JudgeQuestion> selectJudgeQuestionsWithRightAnswer(int paperId);

    @Select("select * from t_judge_question where course = #{courseName}")
    List<JudgeQuestion> selectAllJudgeQuestionsByCourseName(String courseName);
}
