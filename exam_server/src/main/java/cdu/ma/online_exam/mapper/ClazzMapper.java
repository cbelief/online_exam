package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.Clazz;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ClazzMapper {
    @Select("select * from t_clazz where clazz_id = #{id}")
    Clazz selectClazzById(int id);
}
