package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.Exam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ExamMapper {

    @Select("<script>" +
                "select * from t_exam" +
                "<where>" +
                    "<if test=\"keyWord !=null and keyWord != ''\">" +
                        "course like '%${keyWord}%' or description like '%${keyWord}%'" +
                    "</if>" +
                "</where>" +
            "</script>")
    List<Exam> selectExams(String keyWord);

    @Select("select * from t_exam where exam_code = #{examCode}")
    Exam selectExamById(int examCode);

}