package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.Major;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface MajorMapper {
    @Select("select * from t_major where major_id = #{id}")
    Major selectMajorById(int id);
}
