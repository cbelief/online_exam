package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.AnswerLog;
import cdu.ma.online_exam.entity.FillQuestion;
import cdu.ma.online_exam.entity.JudgeQuestion;
import cdu.ma.online_exam.entity.MultiQuestion;
import cdu.ma.online_exam.vo.ExamLog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
@Mapper
public interface StudentExamLogMapper {

    @Insert("insert into t_exam_record values(null, #{examCode}, #{studentId}, #{score})")
    @Options(useGeneratedKeys = true, keyProperty = "examRecordId", keyColumn = "exam_record_id")
    void insertExamLog(Map<String, Integer> map);

    @Insert("<script>" +
            "insert into t_student_exam_answer_record values" +
            "<foreach collection='answerLogs' item='answerLog' separator=','>" +
            "(null, #{answerLog.questionId}, #{answerLog.questionType}, #{examRecordId}, #{answerLog.studentAnswer}, #{answerLog.rightAnswer})" +
            "</foreach>" +
            "</script>")
    void insertStudentAnswerLog(@Param("answerLogs") List<AnswerLog> answerLogs, @Param("examRecordId") int examRecordId);

    @Select("<script>" +
            "select e.exam_code, e.course as courseName, e.description, e.exam_date, er.exam_record_id, er.score " +
            "from t_exam_record er join t_exam e on er.exam_code = e.exam_code " +
            "<where>" +
            "<if test=\"keyWord !=null and keyWord != ''\">" +
            "e.course like '%${keyWord}%' or e.description like '%${keyWord}%'" +
            "</if>" +
            " and er.student_id = #{studentId}" +
            "</where>" +
            "</script>")
    List<ExamLog> selectExamLogs(@Param("keyWord") String keyWord, @Param("studentId") int studentId);

    @Select("select r.question_id,r.student_answer,r.right_answer,q.question,q.analysis,q.optionA,q.optionB,q.optionC,q.optionD " +
            "from t_student_exam_answer_record r " +
            "join t_multi_question q " +
            "on r.question_id = q.question_id " +
            "where exam_record_id=#{examRecordId} and question_type=1")
    List<MultiQuestion> selectMultiQuestionsLog(int examRecordId);

    @Select("select r.question_id,r.student_answer, r.right_answer,q.question,q.analysis " +
            "from t_student_exam_answer_record r " +
            "join t_fill_question q " +
            "on r.question_id = q.question_id " +
            "where exam_record_id=#{examRecordId} and question_type=2")
    List<FillQuestion> selectFillQuestionsLog(int examRecordId);

    @Select("select r.question_id,r.student_answer, r.right_answer,q.question,q.analysis " +
            "from t_student_exam_answer_record r " +
            "join t_judge_question q " +
            "on r.question_id = q.question_id " +
            "where exam_record_id=#{examRecordId} and question_type=3")
    List<JudgeQuestion> selectJudgeQuestionsLog(int examRecordId);
}
