package cdu.ma.online_exam.mapper;

import cdu.ma.online_exam.entity.AnswerLog;
import cdu.ma.online_exam.entity.FillQuestion;
import cdu.ma.online_exam.entity.JudgeQuestion;
import cdu.ma.online_exam.entity.MultiQuestion;
import cdu.ma.online_exam.vo.ExamLog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface StudentPracticeLogMapper {
    @Insert("insert into t_practice_record values(null, #{studentId}, #{courseName}, #{score}, #{createTime})")
    @Options(useGeneratedKeys = true, keyProperty = "practiceRecordId",keyColumn = "practice_record_id")
    void insertPracticeLog(Map<String,Object> map);

    @Insert("<script>" +
            "insert into t_student_practice_answer_record values" +
            "<foreach collection='answerLogs' item='answerLog' separator=','>" +
            "(null, #{answerLog.questionId}, #{answerLog.questionType}, #{practiceRecordId}, #{answerLog.studentAnswer}, #{answerLog.rightAnswer})" +
            "</foreach>" +
            "</script>")
    void insertStudentAnswerLog(@Param("answerLogs") List<AnswerLog> answerLogs, @Param("practiceRecordId") int practiceRecordId);

    @Select("<script>" +
            "select practice_record_id as practiceRecordId, course_name as courseName, score, create_time as createTime " +
            "from t_practice_record" +
            "<where>" +
            "<if test=\"keyWord !=null and keyWord != ''\">" +
            "course_name like '%${keyWord}%' or create_time like '%${keyWord}%'" +
            "</if>" +
            " and student_id = #{studentId}" +
            "</where>" +
            "</script>")
    List<Map<String, Object>> selectPracticeLogs(@Param("keyWord") String keyWord, @Param("studentId") int studentId);

    @Select("select r.question_id,r.student_answer,r.right_answer,q.question,q.analysis,q.optionA,q.optionB,q.optionC,q.optionD " +
            "from t_student_practice_answer_record r " +
            "join t_multi_question q " +
            "on r.question_id = q.question_id " +
            "where practice_record_id=#{practiceRecordId} and question_type=1")
    List<MultiQuestion> selectMultiQuestionsLog(int practiceRecordId);

    @Select("select r.question_id,r.student_answer, r.right_answer,q.question,q.analysis " +
            "from t_student_practice_answer_record r " +
            "join t_fill_question q " +
            "on r.question_id = q.question_id " +
            "where practice_record_id=#{practiceRecordId} and question_type=2")
    List<FillQuestion> selectFillQuestionsLog(int practiceRecordId);

    @Select("select r.question_id,r.student_answer, r.right_answer,q.question,q.analysis " +
            "from t_student_practice_answer_record r " +
            "join t_judge_question q " +
            "on r.question_id = q.question_id " +
            "where practice_record_id=#{practiceRecordId} and question_type=3")
    List<JudgeQuestion> selectJudgeQuestionsLog(int practiceRecordId);
}
