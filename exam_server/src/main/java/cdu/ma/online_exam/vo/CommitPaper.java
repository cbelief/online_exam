package cdu.ma.online_exam.vo;

import cdu.ma.online_exam.entity.Paper;
import lombok.Data;

@Data
public class CommitPaper {

    private Paper paper;

    private int studentId;

    private int examCode;

}
