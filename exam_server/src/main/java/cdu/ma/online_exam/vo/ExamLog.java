package cdu.ma.online_exam.vo;

import lombok.Data;

@Data
public class ExamLog {
    private int examCode;
    private String courseName;
    private String description;
    private String examDate;
    private int examRecordId;
    private int score;
}
