package cdu.ma.online_exam.vo;

import cdu.ma.online_exam.entity.FillQuestion;
import cdu.ma.online_exam.entity.JudgeQuestion;
import cdu.ma.online_exam.entity.MultiQuestion;
import lombok.Data;

import java.util.List;

@Data
public class CommitPractice {
    private List<MultiQuestion> multi;
    private List<FillQuestion> fill;
    private List<JudgeQuestion> judge;
    private int studentId;
    private int score;
    private String courseName;
}
