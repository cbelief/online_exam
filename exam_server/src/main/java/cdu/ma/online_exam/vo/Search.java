package cdu.ma.online_exam.vo;

import lombok.Data;

@Data
public class Search {

    private int pageNum;
    private int pageSize;
    private String sort;
    private String direction;
    private String keyWord;

}

