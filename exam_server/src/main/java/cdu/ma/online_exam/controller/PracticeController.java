package cdu.ma.online_exam.controller;

import cdu.ma.online_exam.entity.Question;
import cdu.ma.online_exam.service.PracticeService;
import cdu.ma.online_exam.vo.CommitPractice;
import cdu.ma.online_exam.vo.Result;
import cdu.ma.online_exam.vo.Search;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api")
public class PracticeController {
    @Autowired
    private PracticeService practiceService;

    @GetMapping("/practice")
    @ResponseBody
    public Result<Map<String, List<Question>>> selectPractice(String courseName) {
        Map<String, List<Question>> questionMap = practiceService.selectPracticeByRand(courseName);
        return new Result(questionMap, "success", 200);
    }

    @PostMapping("/practice/commit")
    @ResponseBody
    public Result commitPractice(@RequestBody CommitPractice commitPractice) {
        practiceService.handleCommitPractice(commitPractice);
        return new Result("练习已提交", "success", 200);
    }

    @PostMapping("/practice/getLogs")
    @ResponseBody
    Result selectPracticeLogs(@RequestBody Search search, @RequestParam("studentId") int studentId) {
        PageInfo<List<Map<String, Object>>> practiceLogs = practiceService.selectPracticeLogs(search, studentId);
        return new Result<>(practiceLogs, "success", 200);
    }

    @GetMapping("/practice/getLogDetail")
    @ResponseBody
    public Result selectExamLogDetail(int practiceRecordId) {
        Map<String, List<? extends Question>> examLogDetail = practiceService.selectPracticeLogDetail(practiceRecordId);
        return new Result(examLogDetail, "success", 200);
    }
}
