package cdu.ma.online_exam.controller;


import cdu.ma.online_exam.entity.Exam;
import cdu.ma.online_exam.entity.Question;
import cdu.ma.online_exam.service.ExamService;
import cdu.ma.online_exam.vo.ExamLog;
import cdu.ma.online_exam.vo.Result;
import cdu.ma.online_exam.vo.Search;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api")
public class ExamController {

    @Autowired
    private ExamService examService;

    @PostMapping("/exams")
    @ResponseBody
    public Result<PageInfo<Exam>> selectExamManage(@RequestBody Search search) {
        int pageNum = search.getPageNum();
        int pageSize = search.getPageSize();
        String keyWord = search.getKeyWord();
        PageInfo<Exam> examManagePageInfo = examService.selectExams(pageNum, pageSize, keyWord);
        if (examManagePageInfo != null) {
            return new Result<>(examManagePageInfo, "success", 200);
        } else {
            return new Result(null, "fail", 200);
        }
    }

    @GetMapping("/exam/{examCode}")
    @ResponseBody
    public Result<Exam> selectExamManageById(@PathVariable("examCode") Integer examCode) {
        Exam exam = examService.selectExamById(examCode);
        if (exam != null) {
            return new Result<>(exam, "success", 200);
        } else {
            return new Result<>(null, "fail", 200);
        }
    }

    @PostMapping("/exam/getLogs")
    @ResponseBody
    public Result<PageInfo<ExamLog>> selectExamLogs(@RequestParam("studentId") int studentId, @RequestBody Search search) {
        int pageNum = search.getPageNum();
        int pageSize = search.getPageSize();
        String keyWord = search.getKeyWord();
        PageInfo<ExamLog> examLogPageInfo = examService.selectExamLogs(pageNum, pageSize, keyWord, studentId);
        return new Result<PageInfo<ExamLog>>(examLogPageInfo, "success", 200);
    }

    @GetMapping("/exam/getLogDetail")
    @ResponseBody
    public Result selectExamLogDetail(int examRecordId) {
        Map<String, List<? extends Question>> examLogDetail = examService.selectExamLogDetail(examRecordId);
        return new Result(examLogDetail, "success", 200);
    }
}
