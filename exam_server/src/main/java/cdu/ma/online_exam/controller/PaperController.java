package cdu.ma.online_exam.controller;

import cdu.ma.online_exam.entity.Paper;
import cdu.ma.online_exam.service.PaperService;
import cdu.ma.online_exam.vo.CommitPaper;
import cdu.ma.online_exam.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api")
public class PaperController {

    @Autowired
    private PaperService paperService;

    @ResponseBody
    @GetMapping("/paper/{paperId}")
    public Result<Paper> selectPaperById(@PathVariable("paperId") int paperId) {
        Paper paper = paperService.selectPaperById(paperId);
        if (paper != null) {
            return new Result(paper, "success", 200);
        } else {
            return new Result(null, "fail", 500);
        }
    }

    @ResponseBody
    @PostMapping("/paper/commit")
    public Result commitPaper(@RequestBody CommitPaper commitPaper) {
        int totalScore = paperService.handleCommitPaper(commitPaper);
        //返回总分
        return new Result<Integer>(totalScore, "success", 200);
    }

}
