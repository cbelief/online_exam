package cdu.ma.online_exam.controller;

import cdu.ma.online_exam.entity.Course;
import cdu.ma.online_exam.service.CourseService;
import cdu.ma.online_exam.vo.Result;
import cdu.ma.online_exam.vo.Search;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api")
public class CourseController {
    @Autowired
    private CourseService courseService;
    @PostMapping("/courses")
    @ResponseBody
    public Result<PageInfo<Course>> selectCourses(@RequestBody Search search){
        int pageNum = search.getPageNum();
        int pageSize = search.getPageSize();
        String keyWord = search.getKeyWord();
        PageInfo<Course> coursePageInfo = courseService.selectCourse(pageNum, pageSize, keyWord);
        if (coursePageInfo != null){
            return new Result<>(coursePageInfo, "success", 200);
        }else {
            return new Result<>(null, "fail", 200);
        }
    }
}

