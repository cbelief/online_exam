package cdu.ma.online_exam.controller;

import cdu.ma.online_exam.entity.Student;
import cdu.ma.online_exam.service.LoginService;
import cdu.ma.online_exam.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller()
@RequestMapping("/api")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @ResponseBody
    @GetMapping("/login")
    Result<Student> login(@RequestParam("studentId") int studentId, @RequestParam("passWord") String passWord){
        Student student =  loginService.login(studentId,passWord);
        if (student != null) {
            return new Result<Student>(student, "登录成功", 200);
        } else {
            return new Result<>(null, "登录失败", 500);
        }
    }
}
