package cdu.ma.online_exam.service.impl;

import cdu.ma.online_exam.entity.Course;
import cdu.ma.online_exam.mapper.CourseMapper;
import cdu.ma.online_exam.service.CourseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseMapper courseMapper;
    @Override
    public PageInfo<Course> selectCourse(int pageNum, int pageSize, String keyWord) {
        PageHelper.startPage(pageNum, pageSize);
        List<Course> courseList = courseMapper.SelectCourses(keyWord);
        PageInfo<Course> coursePageInfo = new PageInfo(courseList);
        return coursePageInfo;
    }
}
