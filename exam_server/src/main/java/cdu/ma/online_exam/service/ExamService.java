package cdu.ma.online_exam.service;

import cdu.ma.online_exam.entity.Exam;
import cdu.ma.online_exam.entity.Question;
import cdu.ma.online_exam.vo.ExamLog;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface ExamService {
    PageInfo<Exam> selectExams(int pageNum, int pageSize, String keyWord);

    Exam selectExamById(int examCode);

    PageInfo<ExamLog> selectExamLogs(int pageNum, int pageSize, String keyWord, int studentId);

    Map<String, List<? extends Question>> selectExamLogDetail(int examRecordId);
}
