package cdu.ma.online_exam.service.impl;

import cdu.ma.online_exam.entity.FillQuestion;
import cdu.ma.online_exam.entity.JudgeQuestion;
import cdu.ma.online_exam.entity.MultiQuestion;
import cdu.ma.online_exam.entity.Question;
import cdu.ma.online_exam.mapper.*;
import cdu.ma.online_exam.service.PracticeService;
import cdu.ma.online_exam.entity.AnswerLog;
import cdu.ma.online_exam.vo.CommitPractice;
import cdu.ma.online_exam.vo.Search;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class PracticeServiceImpl implements PracticeService {
    @Autowired
    private MultiQuestionMapper multiQuestionMapper;
    @Autowired
    private FillQuestionMapper fillQuestionMapper;
    @Autowired
    private JudgeQuestionMapper judgeQuestionMapper;
    @Autowired
    private StudentPracticeLogMapper studentPracticeLogMapper;

    @Override
    public Map<String, List<Question>> selectPracticeByRand(String courseName) {
        //通过课程名获取该课程所有题目
        List<? extends Question> multiQuestions = multiQuestionMapper.selectAllMultiQuestionsByCourseName(courseName);
        List<? extends Question> fillQuestions = fillQuestionMapper.selectAllFillQuestionsByCourseName(courseName);
        List<? extends Question> judgeQuestions = judgeQuestionMapper.selectAllJudgeQuestionsByCourseName(courseName);
        //随机抽取四道选择题，两道填空题，两道判断题
        int multiCount = 4;
        int fillCount = 2;
        int judgeCount = 2;
        //通过生成随机数来随机地选择列表下标，达到随机抽题的效果
        //定义保存三种题型对应下标的数组
        int[] multiQuestionOptions = new int[multiCount];
        int[] fillQuestionOptions = new int[fillCount];
        int[] judgeQuestionOptions = new int[judgeCount];

        //生成选择题列表的随机下标并存入数组
        generateRandomIndex(multiQuestions.size(), 4, multiQuestionOptions);
        //生成填空题列表的随机下标
        generateRandomIndex(fillQuestions.size(), 2, fillQuestionOptions);
        //生成判断题列表的随机下标
        generateRandomIndex(judgeQuestions.size(), 2, judgeQuestionOptions);
        //通过数组取出下标，将随机下标对应的题目加入练习题
        List<Question> multi = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            Question multiQuestion = multiQuestions.get(multiQuestionOptions[i]);
            multi.add(multiQuestion);
        }
        List<Question> fill = new ArrayList<>(2);
        for (int i = 0; i < 2; i++) {
            Question fillQuestion = fillQuestions.get(fillQuestionOptions[i]);
            fill.add(fillQuestion);
        }
        List<Question> judge = new ArrayList<>(2);
        for (int i = 0; i < 2; i++) {
            Question judgeQuestion = judgeQuestions.get(judgeQuestionOptions[i]);
            judge.add(judgeQuestion);
        }

        Map<String, List<Question>> questionsMap = new HashMap<>();
        questionsMap.put("multi", multi);
        questionsMap.put("fill", fill);
        questionsMap.put("judge", judge);
        return questionsMap;
    }

    @Override
    @Transactional
    public void handleCommitPractice(CommitPractice commitPractice) {
        int studentId = commitPractice.getStudentId();
        int score = commitPractice.getScore();
        String courseName = commitPractice.getCourseName();
        List<MultiQuestion> multiQuestions = commitPractice.getMulti();
        List<FillQuestion> fillQuestions = commitPractice.getFill();
        List<JudgeQuestion> judgeQuestions = commitPractice.getJudge();

        List<AnswerLog> answerLogs = new ArrayList<>();

        for (MultiQuestion multiQuestion : multiQuestions) {
            AnswerLog answerLog = new AnswerLog(multiQuestion.getQuestionId(), 1, multiQuestion.getStudentAnswer(), multiQuestion.getRightAnswer());
            answerLogs.add(answerLog);
        }
        for (FillQuestion fillQuestion : fillQuestions) {
            AnswerLog answerLog = new AnswerLog(fillQuestion.getQuestionId(), 2, fillQuestion.getStudentAnswer(), fillQuestion.getRightAnswer());
            answerLogs.add(answerLog);
        }
        for (JudgeQuestion judgeQuestion : judgeQuestions) {
            AnswerLog answerLog = new AnswerLog(judgeQuestion.getQuestionId(), 3, judgeQuestion.getStudentAnswer(), judgeQuestion.getRightAnswer());
            answerLogs.add(answerLog);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("practiceRecordId", 0);
        map.put("studentId", studentId);
        map.put("courseName", courseName);
        map.put("score", score);
        LocalDateTime now = LocalDateTime.now();
        String dateTimeFormat = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm-ss"));
        map.put("createTime", dateTimeFormat);
        //插入练习记录
        studentPracticeLogMapper.insertPracticeLog(map);

        Integer examRecordId = (Integer) map.get("practiceRecordId");
        //插入所有题目的做题记录
        studentPracticeLogMapper.insertStudentAnswerLog(answerLogs, examRecordId);
    }

    @Override
    public PageInfo<List<Map<String, Object>>>  selectPracticeLogs(Search search, int studentId) {
        String keyWord = search.getKeyWord();
        int pageNum = search.getPageNum();
        int pageSize = search.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String, Object>> practiceLogs = studentPracticeLogMapper.selectPracticeLogs(keyWord, studentId);
        PageInfo<List<Map<String, Object>>> practiceLogPageInfo = new PageInfo(practiceLogs);
        return practiceLogPageInfo;
    }

    @Override
    public Map<String, List<? extends Question>> selectPracticeLogDetail(int practiceRecordId) {
        List<MultiQuestion> multiQuestions = studentPracticeLogMapper.selectMultiQuestionsLog(practiceRecordId);
        List<FillQuestion> fillQuestions = studentPracticeLogMapper.selectFillQuestionsLog(practiceRecordId);
        List<JudgeQuestion> judgeQuestions = studentPracticeLogMapper.selectJudgeQuestionsLog(practiceRecordId);
        Map<String,List<? extends Question>> questionMap = new HashMap();
        questionMap.put("multi",multiQuestions);
        questionMap.put("fill",fillQuestions);
        questionMap.put("judge",judgeQuestions);
        return questionMap;
    }


    private void generateRandomIndex(int questionsSize, int count, int[] questionOptions) {
        int questionOptionsIndex = 0;
        Random random = new Random();
        while (count > 0) {
            boolean isRepeat = false;
            int randomIndex = random.nextInt(questionsSize);
            if (questionOptionsIndex == 0) {
                questionOptions[questionOptionsIndex] = randomIndex;
                questionOptionsIndex++;
                count--;
                continue;
            }
            for (int i = questionOptionsIndex; i >= 0; i--) {
                if (randomIndex == questionOptions[i]) {
                    isRepeat = true;
                    break;
                }
            }
            if (isRepeat == true) {
                continue;
            } else {
                questionOptions[questionOptionsIndex] = randomIndex;
                questionOptionsIndex++;
                count--;
            }
        }
    }

}

