package cdu.ma.online_exam.service.impl;

import cdu.ma.online_exam.entity.*;
import cdu.ma.online_exam.mapper.ExamMapper;
import cdu.ma.online_exam.mapper.StudentExamLogMapper;
import cdu.ma.online_exam.service.ExamService;
import cdu.ma.online_exam.vo.ExamLog;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExamServiceImpl implements ExamService {

    @Autowired
    private ExamMapper examMapper;
    @Autowired
    private StudentExamLogMapper studentExamLogMapper;

    @Override
    public PageInfo<Exam> selectExams(int pageNum, int pageSize, String keyWord) {
        PageHelper.startPage(pageNum, pageSize);
        List<Exam> examList = examMapper.selectExams(keyWord);
        PageInfo<Exam> examManagePageInfo = new PageInfo<>(examList);
        return examManagePageInfo;
    }

    @Override
    public Exam selectExamById(int examCode) {
        return examMapper.selectExamById(examCode);
    }

    @Override
    public PageInfo<ExamLog> selectExamLogs(int pageNum, int pageSize, String keyWord, int studentId) {
        PageHelper.startPage(pageNum, pageSize);
        List<ExamLog> examLogs = studentExamLogMapper.selectExamLogs(keyWord, studentId);
        PageInfo<ExamLog> examLogPageInfo = new PageInfo<>(examLogs);
        return examLogPageInfo;
    }

    @Override
    public Map<String, List<? extends Question>> selectExamLogDetail(int examRecordId) {
        List<MultiQuestion> multiQuestions = studentExamLogMapper.selectMultiQuestionsLog(examRecordId);
        List<FillQuestion> fillQuestions = studentExamLogMapper.selectFillQuestionsLog(examRecordId);
        List<JudgeQuestion> judgeQuestions = studentExamLogMapper.selectJudgeQuestionsLog(examRecordId);
        Map<String,List<? extends Question>> questionMap = new HashMap();
        questionMap.put("multi",multiQuestions);
        questionMap.put("fill",fillQuestions);
        questionMap.put("judge",judgeQuestions);
        return questionMap;
    }

}
