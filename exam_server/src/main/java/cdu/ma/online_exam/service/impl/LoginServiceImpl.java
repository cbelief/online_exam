package cdu.ma.online_exam.service.impl;

import cdu.ma.online_exam.entity.Student;
import cdu.ma.online_exam.mapper.StudentMapper;
import cdu.ma.online_exam.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Student login(int studentId, String passWord) {
        return studentMapper.selectStudentByIdAndPwd(studentId, passWord);
    }
}
