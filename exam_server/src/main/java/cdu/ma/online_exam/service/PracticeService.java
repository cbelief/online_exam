package cdu.ma.online_exam.service;

import cdu.ma.online_exam.entity.Question;
import cdu.ma.online_exam.vo.CommitPractice;
import cdu.ma.online_exam.vo.Search;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface PracticeService {
    Map<String, List<Question>> selectPracticeByRand(String courseName);

    void handleCommitPractice(CommitPractice commitPractice);

    PageInfo<List<Map<String, Object>>> selectPracticeLogs(Search search, int studentId);

    Map<String, List<? extends Question>> selectPracticeLogDetail(int practiceRecordId);
}
