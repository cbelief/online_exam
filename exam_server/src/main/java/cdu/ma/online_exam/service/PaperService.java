package cdu.ma.online_exam.service;

import cdu.ma.online_exam.entity.Paper;
import cdu.ma.online_exam.entity.Question;
import cdu.ma.online_exam.vo.CommitPaper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface PaperService {
    Paper selectPaperById(int paperId);

    Paper selectPaperWithRightAnswer(int paperId);

    int handleCommitPaper(CommitPaper commitPaper);
}
