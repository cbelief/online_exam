package cdu.ma.online_exam.service;

import cdu.ma.online_exam.vo.Result;
import cdu.ma.online_exam.entity.Student;

public interface LoginService {
    Student login(int studentId, String passWord);
}
