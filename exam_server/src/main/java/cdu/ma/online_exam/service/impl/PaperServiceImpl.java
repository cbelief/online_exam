package cdu.ma.online_exam.service.impl;

import cdu.ma.online_exam.entity.*;
import cdu.ma.online_exam.mapper.FillQuestionMapper;
import cdu.ma.online_exam.mapper.JudgeQuestionMapper;
import cdu.ma.online_exam.mapper.MultiQuestionMapper;
import cdu.ma.online_exam.mapper.StudentExamLogMapper;
import cdu.ma.online_exam.service.PaperService;
import cdu.ma.online_exam.entity.AnswerLog;
import cdu.ma.online_exam.vo.CommitPaper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class PaperServiceImpl implements PaperService {
    @Autowired
    private MultiQuestionMapper multiQuestionMapper;
    @Autowired
    private FillQuestionMapper fillQuestionMapper;
    @Autowired
    private JudgeQuestionMapper judgeQuestionMapper;
    @Autowired
    private StudentExamLogMapper studentExamLogMapper;

    @Override
    public Paper selectPaperById(int paperId) {
        List<MultiQuestion> multiQuestions = multiQuestionMapper.selectMultiQuestionsByPaperID(paperId);
        List<FillQuestion> fillQuestions = fillQuestionMapper.selectFillQuestionsByPaperID(paperId);
        List<JudgeQuestion> judgeQuestions = judgeQuestionMapper.selectJudgeQuestionsByPaperID(paperId);
        if (multiQuestions != null && fillQuestions != null && judgeQuestions != null) {
            Paper paper = new Paper();
            paper.setMulti(multiQuestions);
            paper.setFill(fillQuestions);
            paper.setJudge(judgeQuestions);
            paper.setPaperId(paperId);
            return paper;
        } else {
            return null;
        }
    }

    @Override
    public Paper selectPaperWithRightAnswer(int paperId) {
        List<MultiQuestion> multiQuestions = multiQuestionMapper.selectMultiQuestionsWithRightAnswer(paperId);
        List<FillQuestion> fillQuestions = fillQuestionMapper.selectFillQuestionsWithRightAnswer(paperId);
        List<JudgeQuestion> judgeQuestions = judgeQuestionMapper.selectJudgeQuestionsWithRightAnswer(paperId);
        Paper paper = new Paper();
        paper.setMulti(multiQuestions);
        paper.setFill(fillQuestions);
        paper.setJudge(judgeQuestions);
        return paper;
    }

    @Override
    @Transactional
    public int handleCommitPaper(CommitPaper commitPaper) {

        //学生作答的试卷每种题型的作答详情
        List<MultiQuestion> multiQuestionsByStudent = commitPaper.getPaper().getMulti();
        List<FillQuestion> fillQuestionsByStudent = commitPaper.getPaper().getFill();
        List<JudgeQuestion> judgeQuestionsByStudent = commitPaper.getPaper().getJudge();

        //带有标准答案的试卷
        Paper paperWithRightAnswer = selectPaperWithRightAnswer(commitPaper.getPaper().getPaperId());
        //带有标准答案的每种题型的详情
        List<MultiQuestion> multiQuestionsByRight = paperWithRightAnswer.getMulti();
        List<FillQuestion> fillQuestionsByRight = paperWithRightAnswer.getFill();
        List<JudgeQuestion> judgeQuestionsByRight = paperWithRightAnswer.getJudge();
        //存储学生答案和标准答案的列表
        List<AnswerLog> answerLogs = new ArrayList<>();

        //计算分数
        int totalScore = 0;

        //计算选择题分数
        Map<Integer, String> studentMultiAnswerMap = new HashMap();
        for (MultiQuestion multiQuestionByStudent : multiQuestionsByStudent) {
            studentMultiAnswerMap.put(multiQuestionByStudent.getQuestionId(), multiQuestionByStudent.getStudentAnswer());
        }
        for (MultiQuestion multiQuestionByRight : multiQuestionsByRight) {
            int questionId = multiQuestionByRight.getQuestionId();
            //拿到对应的学生作答，标准答案和解析
            String studentAnswer = studentMultiAnswerMap.get(questionId);
            String rightAnswer = multiQuestionByRight.getRightAnswer();
            //记录学生作答和标准答案
            AnswerLog answerLog = new AnswerLog(questionId, 1, studentAnswer, rightAnswer);
            answerLogs.add(answerLog);
            //算分
            if (rightAnswer.equals(studentAnswer)) {
                totalScore += multiQuestionByRight.getScore();
            }
        }

        //计算填空题分数
        Map<Integer, String> studentFillAnswerMap = new HashMap();
        for (FillQuestion fillQuestionByStudent : fillQuestionsByStudent) {
            studentFillAnswerMap.put(fillQuestionByStudent.getQuestionId(), fillQuestionByStudent.getStudentAnswer());
        }
        for (FillQuestion fillQuestionByRight : fillQuestionsByRight) {
            int questionId = fillQuestionByRight.getQuestionId();
            //拿到对应的学生作答和标准答案
            String studentAnswer = studentFillAnswerMap.get(questionId);
            String rightAnswer = fillQuestionByRight.getRightAnswer();
            //记录学生作答和标准答案
            AnswerLog answerLog = new AnswerLog(questionId, 2, studentAnswer, rightAnswer);
            answerLogs.add(answerLog);
            //算分
            if (rightAnswer.equals(studentAnswer)) {
                totalScore += fillQuestionByRight.getScore();
            }
        }

        //计算判断题分数
        Map<Integer, String> studentJudgeAnswerMap = new HashMap();
        for (JudgeQuestion judgeQuestionByStudent : judgeQuestionsByStudent) {
            studentJudgeAnswerMap.put(judgeQuestionByStudent.getQuestionId(), judgeQuestionByStudent.getStudentAnswer());
        }
        for (JudgeQuestion judgeQuestionByRight : judgeQuestionsByRight) {
            Integer questionId = judgeQuestionByRight.getQuestionId();
            // 拿到对应的学生作答和标准答案
            String studentAnswer = studentJudgeAnswerMap.get(questionId);
            String rightAnswer = judgeQuestionByRight.getRightAnswer();
            //记录学生作答和标准答案
            AnswerLog answerLog = new AnswerLog(questionId, 3, studentAnswer, rightAnswer);
            answerLogs.add(answerLog);
            //算分
            if (rightAnswer.equals(studentAnswer)) {
                totalScore += judgeQuestionByRight.getScore();
            }
        }

        int examCode = commitPaper.getExamCode();
        int studentId = commitPaper.getStudentId();
        Map<String, Integer> map = new HashMap<>();
        map.put("examRecordId", 0);
        map.put("examCode", examCode);
        map.put("studentId", studentId);
        map.put("score", totalScore);
        //插入考试记录
        studentExamLogMapper.insertExamLog(map);
        Integer examRecordId = map.get("examRecordId");
        //插入所有题目的做题记录
        studentExamLogMapper.insertStudentAnswerLog(answerLogs, examRecordId);
        //返回总分
        return totalScore;
    }

}
