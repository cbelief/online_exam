package cdu.ma.online_exam.service;

import cdu.ma.online_exam.entity.Course;
import com.github.pagehelper.PageInfo;

public interface CourseService {
    PageInfo<Course> selectCourse(int pageNum, int pageSize, String keyWord);
}
